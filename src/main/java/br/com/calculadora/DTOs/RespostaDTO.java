package br.com.calculadora.DTOs;

public class RespostaDTO {
    private Integer resultadoint;
    private Double resultadoDouble;

    public RespostaDTO() {
    }

    public RespostaDTO(Integer resultadoint) {
        this.resultadoint = resultadoint;
    }

    public RespostaDTO(Double resultadoDouble) {
        this.resultadoDouble = resultadoDouble;
    }

    public Integer getResultado() {
        return resultadoint;
    }

    public void setResultado(Integer resultado) {
        this.resultadoint = resultado;
    }

    public Double getResultadoDouble() {
        return resultadoDouble;
    }

    public void setResultadoDouble(Double resultadoDouble) {
        this.resultadoDouble = resultadoDouble;
    }
}
