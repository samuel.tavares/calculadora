package br.com.calculadora.controllers;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Necessário pelo menos dois números");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Necessário pelo menos dois numeros");
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Necessário pelo menos dois numeros");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1 ||calculadora.getNumeros().size() > 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Obrigatório dois numeros");
        }

        int quantidadeDeZeros = 0;
        for(int numero: calculadora.getNumeros()){
            if(numero == 0){
                quantidadeDeZeros += 1;
            }
        }
        if (quantidadeDeZeros >= calculadora.getNumeros().size()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parâmetro inválido: divisão por zero");
        }
        return calculadoraService.dividir(calculadora);
    }

}
