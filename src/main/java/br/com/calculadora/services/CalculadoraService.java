package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalculadoraService {

    public RespostaDTO somar (Calculadora calculadora){
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            resultado = resultado + numero;
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;
    }

    public RespostaDTO subtrair (Calculadora calculadora){
        int resultado = calculadora.getNumeros().get(0);
        for(int i=1; i < calculadora.getNumeros().size(); i++){
            resultado = resultado-calculadora.getNumeros().get(i);
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;
    }

    public RespostaDTO multiplicar(Calculadora calculadora){
        int resultado = calculadora.getNumeros().get(0);
        for (int i=1; i < calculadora.getNumeros().size(); i++){
            resultado = resultado*calculadora.getNumeros().get(i);
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;
    }

    public RespostaDTO dividir(Calculadora calculadora){
        double resultado;
        if(calculadora.getNumeros().get(0) > calculadora.getNumeros().get(1)){
            resultado = calculadora.getNumeros().get(0) / calculadora.getNumeros().get(1);
        }
        else{
            resultado = calculadora.getNumeros().get(1) / calculadora.getNumeros().get(0);
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;
    }
}
